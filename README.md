# Anicetus

## Dependencies

```bash
apt install php-fpm php-intl php-xml php-gmp php-curl php-postgresql postgresql
```

Install [composer](https://getcomposer.org/download/) on your system.

Create a PostgreSQL user and a database for anicetus.

## Installation

```bash
cd /var/www
git clone https://framagit.org/framasoft/framatalk/anicetus.git
cd anicetus
SYMFONY_ENV=prod composer install --no-dev --optimize-autoloader
git submodule init
git submodule update
cp .env .env.local
SYMFONY_ENV=prod composer dump-env prod
```

Edit `.env.local`.

```bash
php bin/console sass:build
php bin/console doctrine:migrations:migrate
chown www-data: -R .
cat <<EOF > /etc/systemd/system/anicetus-async.service
[Unit]
Description=Anicetus Async Service
After=network.target postgresql.service

[Service]
User=www-data
WorkingDirectory=/var/www/anicetus
ExecStart=/usr/bin/php8.2 bin/console messenger:consume async --time-limit=3600
KillMode=process
Restart=always
RestartSec=30

SyslogIdentifier=anicetus-async


[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now anicetus-async.service
```

## License

Anicetus is licensed under the terms of the GNU Affero GPLv3. See [LICENSE](LICENSE) file.
