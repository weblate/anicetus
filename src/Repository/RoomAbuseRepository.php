<?php

namespace App\Repository;

use App\Entity\RoomAbuse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RoomAbuse>
 *
 * @method RoomAbuse|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomAbuse|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomAbuse[]    findAll()
 * @method RoomAbuse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomAbuseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoomAbuse::class);
    }

    /**
     * @throws Exception
     */
    public function findRoomAbuse(string $roomName, string $userIP)
    {
        $em = $this->getEntityManager();

        $sql = '
            SELECT * from room_abuse ra
            WHERE room_name = :roomName
            OR :userIP::inet <<= ip
            ORDER BY created_at
            LIMIT 1;
        ';
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(RoomAbuse::class, 'ra');
        $query = $em->createNativeQuery($sql, $rsm);
        $query->setParameter(':roomName', $roomName);
        $query->setParameter(':userIP', $userIP);
        return $query->getOneOrNullResult();
    }

    //    /**
    //     * @return RoomAbuse[] Returns an array of RoomAbuse objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?RoomAbuse
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
