<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\RoomSessionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RoomSessionRepository::class)]
class RoomSession
{
    use TimeStampableTrait;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
        $this->roomAbuses = new ArrayCollection();
    }


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: false)]
    #[Assert\Length(min: 4, max: 255)]
    private ?string $roomName = null;

    #[ORM\ManyToOne(inversedBy: 'createdRoomSessions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $creator = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Ip(version: 'all_public')]
    private ?string $ip = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\NotBlank]
    private ?string $userAgent = null;

    #[ORM\OneToMany(targetEntity: RoomAbuse::class, mappedBy: 'roomSession', orphanRemoval: true)]
    private Collection $roomAbuses;

    #[ORM\Column(options: ["default" => false])]
    private ?bool $moderated = false;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomName(): ?string
    {
        return $this->roomName;
    }

    public function setRoomName(string $roomName): static
    {
        $this->roomName = $roomName;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): static
    {
        $this->creator = $creator;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): static
    {
        $this->ip = $ip;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    public function setUserAgent(?string $userAgent): static
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * @return Collection<int, RoomAbuse>
     */
    public function getRoomAbuses(): Collection
    {
        return $this->roomAbuses;
    }

    public function addRoomAbuse(RoomAbuse $roomAbuse): static
    {
        if (!$this->roomAbuses->contains($roomAbuse)) {
            $this->roomAbuses->add($roomAbuse);
            $roomAbuse->setRoomSession($this);
        }

        return $this;
    }

    public function removeRoomAbuse(RoomAbuse $roomAbuse): static
    {
        if ($this->roomAbuses->removeElement($roomAbuse)) {
            // set the owning side to null (unless already changed)
            if ($roomAbuse->getRoomSession() === $this) {
                $roomAbuse->setRoomSession(null);
            }
        }

        return $this;
    }

    public function isModerated(): ?bool
    {
        return $this->moderated;
    }

    public function setModerated(bool $moderated): static
    {
        $this->moderated = $moderated;

        return $this;
    }
}
