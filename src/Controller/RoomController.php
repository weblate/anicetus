<?php

namespace App\Controller;

use Ahc\Jwt\JWT;
use App\Entity\RoomSession;
use App\Form\RoomNameType;
use App\Repository\RoomAbuseRepository;
use App\Repository\RoomSessionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Uid\Uuid;

class RoomController extends AbstractController
{
    #[Route('/', name: 'app_room')]
    public function index(Request $request): Response
    {
        $numberOfChars = $this->getParameter('app.service.room.suggested_name.nb_characters') ?? 10;
        $randomRoomName = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", $numberOfChars)), 0, $numberOfChars);
        $form = $this->createForm(RoomNameType::class, ['defaultRoomName' => $randomRoomName]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('room_auth', ['roomName' => $form->get('roomName')->getData()]);
        }
        return $this->render('room/index.html.twig', [
            'randomRoomName' => $randomRoomName,
            'roomNameForm' => $form
        ]);
    }

    #[Route('/room/moderated/new', name: 'new_authenticated_room')]
    public function createModeratedRoom(Request $request, EntityManagerInterface $entityManager): Response
    {
        $meetingId = str_replace('-', '', Uuid::v4() . Uuid::v4());

        $session = $request->getSession();
        $session->set('new_moderated_room', true);

        $roomName = hash('sha256', $meetingId);

        $roomSession = new RoomSession();
        $roomSession->setRoomName($roomName);
        $roomSession->setUserAgent($request->headers->get('User-Agent'));
        $roomSession->setIp($request->getClientIp());
        $roomSession->setCreator($this->getUser());
        $roomSession->setModerated(true);
        $entityManager->persist($roomSession);
        $entityManager->flush();

        return $this->redirectToRoute('room_moderated', ['meetingId' => $meetingId]);
    }

    #[Route('/room/moderated/{meetingId}', name: 'room_moderated')]
    public function showModeratedRoom(string $meetingId, Request $request): Response
    {
        $jitsiDomain = $this->getParameter('app.jitsi.domain');
        $roomName = hash('sha256', $meetingId);

        $session = $request->getSession();
        $newModeratedRoom = $session->get('new_moderated_room', false);
        $session->remove('new_moderated_room');

        return $this->render('room/moderated.html.twig', [
            'guestFullLink' => "https://" . $jitsiDomain . "/" . $roomName,
            'roomName' => $roomName,
            'meetingId' => $meetingId,
            'newModeratedRoom' => $newModeratedRoom === true,
        ]);
    }

    #[Route('/room/{roomName}/auth', name: 'room_auth')]
    public function forwardRoomAuth(string $roomName, Request $request, EntityManagerInterface $entityManager, RoomSessionRepository $roomSessionRepository, RoomAbuseRepository $roomAbuseRepository, RateLimiterFactory $roomAuthLimiter): Response
    {
        $limiter = $roomAuthLimiter->create($request->getClientIp());

        if (false === $limiter->consume(1)->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        $ip = $request->getClientIp();
        $existingAbuse = $roomAbuseRepository->findRoomAbuse($roomName, $ip);

        if ($existingAbuse) {
            return $this->redirect($existingAbuse->getRedirectUrl());
        }

        $jitsiDomain = $this->getParameter('app.jitsi.domain');

        $moderatedRoomSession = $roomSessionRepository->findBy(
            ['roomName' => $roomName, 'moderated' => true],
            ['createdAt' => 'DESC'],
            1
        );

        $meetingId = $request->query->get('meetingId');

        if ($moderatedRoomSession && (!$meetingId || hash('sha256', $meetingId) !== $roomName)) {
            // If we don't provide the MeetingID, it's because we came directly from Jitsi
            return $this->render('room/not_allowed.html.twig', [
                'roomGuestFullLink' => "https://" . $jitsiDomain . "/" . $roomName
            ]);
        }

        $roomSession = new RoomSession();
        $roomSession->setRoomName($roomName);
        $roomSession->setUserAgent($request->headers->get('User-Agent'));
        $roomSession->setIp($request->getClientIp());
        $roomSession->setCreator($this->getUser());
        $entityManager->persist($roomSession);
        $entityManager->flush();

        if ($roomSession->getCreator() === $this->getUser()) {
            $creator = $roomSession->getCreator();

            $jitsiAppIdentifier = $this->getParameter('app.jitsi.appIdentifier');
            $jitsiAppId = $this->getParameter('app.jitsi.appId');
            $jitsiAppSecret = $this->getParameter('app.jitsi.appSecret');
            $jitsiTokenExpiration = $this->getParameter('app.jitsi.tokenExpiration');

            $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $creator->getEmail() ) ) ) . "?d=mp";

            $context = [
                'user' => [
                    'id' => $creator->getId(),
                    'name' => $creator->getUserName(),
                    'avatar' => $grav_url
                ],
                'callee' => [
                    'id' => $creator->getId(),
                    'name' => $creator->getUserName(),
                    'avatar' => $grav_url
                ]
            ];

            $jwt = new JWT($jitsiAppSecret, 'HS256');
            $token = $jwt->encode(
                [
                    'context' => $context,
                    'aud' => $jitsiAppIdentifier,
                    'iss' => $jitsiAppId,
                    'sub' => $jitsiDomain,
                    'room' => $roomSession->getRoomName(),
                    'exp' => time() + $jitsiTokenExpiration * 60 * 60,
                ]
            );

            return $this->redirect("https://" . $jitsiDomain . "/" . $roomSession->getRoomName() . "?jwt=" . $token);
        }

        $this->addFlash('error', 'You are not the creator of this room.');
        return $this->redirectToRoute('app_room');
    }
}
