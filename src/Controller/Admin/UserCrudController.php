<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use Symfony\Component\Translation\TranslatableMessage;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular(new TranslatableMessage('admin.crud.user.singular'))
            ->setEntityLabelInPlural(new TranslatableMessage('admin.crud.user.plural'))
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('username')->setRequired(true)->setLabel(new TranslatableMessage('admin.crud.user.username')),
            EmailField::new('email')->setLabel(new TranslatableMessage('admin.crud.user.email')),
            UrlField::new('avatar')->hideOnIndex()->setLabel(new TranslatableMessage('admin.crud.user.avatar')),
            ChoiceField::new('roles')
                ->allowMultipleChoices()
                ->renderAsBadges()
                ->renderExpanded()
                ->setChoices([
                'Admin' => 'ROLE_ADMIN'
            ])
                ->setLabel(new TranslatableMessage('admin.crud.user.roles')),
            BooleanField::new('isVerified')->setLabel(new TranslatableMessage('admin.crud.user.is_verified')),
            BooleanField::new('isSuspended')->setLabel(new TranslatableMessage('admin.crud.user.is_suspended'))
        ];
    }
}
