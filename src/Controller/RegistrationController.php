<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use function Symfony\Component\Translation\t;

class RegistrationController extends AbstractController
{
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager, UrlMatcherInterface $urlMatcher, TranslatorInterface $translator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $fromAddress = $this->getParameter('app.mail.from.address');
            $fromName = $this->getParameter('app.mail.from.name');
            $serviceName = $this->getParameter('app.service.name');

            $roomName = null;

            // Get the referer, verify it matches and put in the email confirmation link
            if ($request->getSession()->has('_security.main.target_path')) {
                $base = $request->getSchemeAndHttpHost().$request->getBaseUrl();
                $url = $request->getSession()->get('_security.main.target_path');
                $path = str_replace($base, '', $url);
                try {
                    $routeInfo = $urlMatcher->match($path);
                    $roomName = $routeInfo['roomName'] ?? null;
                } catch (ResourceNotFoundException) {
                    // Do nothing
                }
            }

            $subject = $translator->trans('registration.confirmation.subject', ['serviceName' => $serviceName]);

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address($fromAddress, $fromName))
                    ->to($user->getEmail())
                    ->subject($subject)
                    ->locale($request->getLocale())
                    ->context(['serviceName' => $serviceName, 'withRedirection' => $roomName !== null])
                    ->textTemplate('registration/confirmation_email.text.twig')
                    ->htmlTemplate('registration/confirmation_email.html.twig'),
                ['roomName' => $roomName]
            );
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_register_pending');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form,
        ]);
    }

    #[Route('/register/pending', name: 'app_register_pending')]
    public function registrationPending(): Response
    {
        return $this->render('registration/pending.html.twig');
    }

    #[Route('/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request, TranslatorInterface $translator, UserRepository $userRepository, Security $security): Response
    {
        $id = $request->query->get('id');

        if (null === $id) {
            return $this->redirectToRoute('app_register');
        }

        $user = $userRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute('app_register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        $security->login($user);

        $roomName = $request->query->get('roomName');
        if ($roomName) {
            return $this->redirectToRoute('room_auth', ['roomName' => $roomName]);
        }

        return $this->redirectToRoute('app_room');
    }
}
