<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => new TranslatableMessage('registration.form.email'),
                'attr' => [
                    'placeholder' => new TranslatableMessage('registration.form.email')
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('userName', TextType::class, [
                'attr' => [
                    'placeholder' => new TranslatableMessage('registration.form.username')
                ],
                'label' => new TranslatableMessage('registration.form.username'),
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'label' => new TranslatableMessage('registration.form.password.label'),
                'attr' => ['autocomplete' => 'new-password', 'placeholder' => new TranslatableMessage('registration.form.password.label')],
                'constraints' => [
                    new NotBlank([
                        'message' => 'registration.form.password.constraints.blank',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'registration.form.password.constraints.length',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => new TranslatableMessage('registration.form.terms.constraint'),
                    ]),
                ],
                'label' => new TranslatableMessage('registration.form.terms.label'),
                'label_html' => true
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'registration.page.form_btn',
                'attr' => [
                    'class' => 'btn btn-primary w-100 py-2 mb-2'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
