<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                        ]),
                    ],
                    'label' => new TranslatableMessage('reset_password.form_password.first.label'),
                    'row_attr' => [
                        'class' => 'form-floating mb-3',
                    ],
                    'attr' => [
                        'placeholder' => new TranslatableMessage('reset_password.form_password.first.label'),
                        'autocomplete' => 'new-password',
                    ],
                ],
                'second_options' => [
                    'label' => new TranslatableMessage('reset_password.form_password.repeat.label'),
                    'row_attr' => [
                        'class' => 'form-floating mb-3',
                    ],
                    'attr' => [
                        'placeholder' => new TranslatableMessage('reset_password.form_password.repeat.label'),
                        'autocomplete' => 'new-password',
                    ],
                ],
                'invalid_message' => new TranslatableMessage('The password fields must match.', [], 'validators'),
                // Instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
