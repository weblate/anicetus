<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\NotBlank;

class ResetPasswordRequestFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => ['autocomplete' => 'email', 'placeholder' => new TranslatableMessage('reset_password.form.email.label')],
                'label' => new TranslatableMessage('reset_password.form.email.label'),
                'constraints' => [
                    new NotBlank([
                        'message' => new TranslatableMessage('reset_password.form.email_empty'),
                    ]),
                ],
                'row_attr' => [
                    'class' => 'form-floating'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
