<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfilePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('currentPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'label' => new TranslatableMessage('profile.edit.password.current.label'),
                'attr' => ['autocomplete' => 'new-password', 'placeholder' => new TranslatableMessage('profile.edit.password.current.label')],
                'constraints' => [
                    new UserPassword([
                        'message' => 'profile.edit.password.current.wrong'
                    ])
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3',
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Please enter a password',
                        ]),
                        new Length([
                            'min' => 6,
                            'minMessage' => 'Your password should be at least {{ limit }} characters',
                            // max length allowed by Symfony for security reasons
                            'max' => 4096,
                        ]),
                    ],
                    'label' => new TranslatableMessage('reset_password.form_password.first.label'),
                    'row_attr' => [
                        'class' => 'form-floating mb-3',
                    ],
                    'attr' => [
                        'placeholder' => new TranslatableMessage('reset_password.form_password.first.label'),
                        'autocomplete' => 'new-password',
                    ],
                ],
                'second_options' => [
                    'label' => new TranslatableMessage('reset_password.form_password.repeat.label'),
                    'row_attr' => [
                        'class' => 'form-floating mb-3',
                    ],
                    'attr' => [
                        'placeholder' => new TranslatableMessage('reset_password.form_password.repeat.label'),
                        'autocomplete' => 'new-password',
                    ],
                ],
                'invalid_message' => new TranslatableMessage('The password fields must match.', [], 'validators'),
                // Instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => new TranslatableMessage('profile.edit.submit')
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
