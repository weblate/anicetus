error:
    403:
        title: 'No access'
        no_access: 'You do not have access to this page.'
    return_homepage: 'Return to the homepage'
    404:
        title: 'Page not found'
        body: "The requested page couldn't be located. Checkout for any URL misspelling."
    409:
        title: 'Too fast'
        body: "You're creating rooms too fast. Slow down."
registration:
    confirmation:
        title: 'Hi! Please confirm your email!'
        please: 'Please confirm your email address by clicking the following link:'
        link: 'Confirm my Email'
        expiration: 'This link will expire in {time}.'
        redirection: 'You will be automatically redirected to your room.'
        end: Cheers!
        subject: 'Verify your registration request to {serviceName}'
    page:
        title: Register
        password: Password
        form_btn: Register
        already_account: 'Already have an account?'
        login_btn: Login
    pending:
        title: 'Confirm your registration'
        desc: 'Please click on the link in the email we have just sent you to validate your account.'
    form:
        email: Email
        username: Username
        terms: { constraint: 'You need to agree to our terms.', label: "I have read and accept <a target='_blank' href=\"https://framasoft.org/cgu/\">Framasoft's Terms and Conditions</a> as well as <a target='_blank' href='https://framasoft.org/moderation'>the Moderation policy</a>" }
        password: { label: Password, constraints: { blank: 'Please enter a password', length: 'Your password should be at least {{ limit }} characters' } }
reset_password:
    check_email:
        title: 'Password Reset Email Sent'
        reset: 'If an account matching your email exists, then an email was just sent that contains a link that you can use to reset your password.'
        expire: 'This link will expire in {time}.'
        not_received: "If you don't receive an email please check your spam folder or try again."
        back: 'Back to the reset password page'
    email:
        subject: 'Your password reset request'
        title: Hi!
        reset: 'To reset your password, please visit the following link:'
        expire: 'This link will expire in {time}.'
        end: Cheers!
        btn: 'Reset password'
    request:
        title: 'Reset your password'
        help: 'Enter your email address, and we will send you a link to reset your password.'
        btn: 'Send password reset email'
    reset:
        title: 'Reset your password'
        btn: 'Reset password'
    form:
        email: { label: Email }
        email_empty: 'Please enter your email'
    form_password:
        first: { label: 'New password' }
        repeat: { label: 'Repeat password' }
room:
    index:
        title: Rooms
        regular_meeting: { title: 'Regular meeting', create_btn: 'Create a room' }
        moderated: { title: 'Moderated Meetings', desc: 'Moderated meetings is a feature that lets you book a meeting URL in advance where you are the only moderator.', create_btn: 'Get me a moderated meeting!' }
        form: { roomName: { label: 'Room name' } }
    moderated:
        title: 'Room Information'
        created: 'Your room was successfully created.'
        invited: 'You have been invited to join an existing room as moderator.'
        desc: "Forward the links below as you wish and join the meeting when it's time!"
        guests: { link: 'Share meeting link for guests' }
        moderators: { link: 'Share this page with other moderators', help: 'Moderators will need to login as well to access the room with moderator rights.', join_btn: 'Join as moderator' }
    not_allowed:
        title: 'Not allowed to join this room as moderator'
        desc: 'You are not allowed to join this room as moderator.'
        back_btn: 'Go back to the room as guest'
security:
    common:
        why_account: { title: 'Why do I need an account to use {serviceName}?', desc: "This service was previously opened to everyone without any need to register. As we've recently had serious abuses reported, we now require people hosting meetings to create an account. People who are not the host and are simply joining a room do not require to be authenticated." }
        how_data: { title: 'How will my data be used?', desc: 'The given details will only be used to associate them with the rooms being used, so that abusers can be identified and blocked easier.' }
login:
    title: 'Log in!'
    desc: 'Please sign in'
    form:
        email: Email
        password: Password
    sign_in_btn: 'Sign in'
    no_account:
        title: 'No account?'
        link: Register
    pw_forgotten:
        title: 'Password forgotten?'
        link: 'Reset your password'
base:
    header:
        logout: Logout
profile:
    edit:
        title: 'Edit profile'
        password:
            title: Change password
            current:
                label: Current password
                wrong: Wrong value for your current password
            changed: Password successfully changed
        submit: Save
        suspended: Your account has been suspended
admin:
    dashboard:
        chart: { empty: 'No charts to show up yet' }
    sidebar:
        dashboard: Dashboard
        users: Users
        room_sessions: 'Room sessions'
        abuses: Abuses
        back_to_user: 'Back to user section'
    crud:
        user: { singular: User, plural: Users, username: Username, email: Email, avatar: Avatar, roles: Roles, is_verified: Verified?, is_suspended: Suspended? }
        room_session: { singular: 'Room session', plural: 'Room sessions', room_name: Room name, creator: Creator, ip: IP Address, user_agent: User-agent, moderated: Moderated?, created_at: Created at, updated_at: Updated at }
        abuse: { singular: Abuse, plural: Abuses, roomName: 'Room name', ip: 'IP address', redirectUrl: 'Redirection URL', note: Note }
